﻿using UnityEngine;

public class DrawSliceLine : MonoBehaviour
{
    public float RayCastDistance = 100f;
    public GameObject WholeApple;
    public string WholeAppleTag = "WholeApple";
    public GameObject HalfApple;
    public string HalfAppleTag = "HalfApple";
    public GameObject QuartreApple;
    public string QuarterAppleTag = "QuartreApple";
    public float TimeBetweenSwipe = 0.5f;

    public float SwipeForce = 200;
    public float SwipeTorque = 200;

    private Plane _plane;
    private bool _allowSwipe = true;
    private float _lastSwpieTime = 0;

    private Vector3 _lastPosition;
    private Vector3 _deltaOfLastPositionAndCurentPosition;

    void Start()
    {
        _plane = new Plane(Camera.main.transform.forward * -1, transform.position);
    }

    void Update()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase != TouchPhase.Moved)
            return;

        //return ray from camera to point on screen
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        if (_plane.Raycast(ray, out float distanceHitRay))
            transform.position = ray.GetPoint(distanceHitRay);

        if (!_allowSwipe && (_lastSwpieTime + TimeBetweenSwipe) > Time.time)
            _allowSwipe = true;

        _deltaOfLastPositionAndCurentPosition = transform.position - _lastPosition;
        _lastPosition = transform.position;
    }

    void FixedUpdate()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase != TouchPhase.Moved)
            return;

        GameObject hitObject = GetTouchObject.GetGameObject3D(Input.GetTouch(0).position, RayCastDistance);

        if (hitObject == null)
            return;

        SplitApple(hitObject);
    }

    private void SplitApple(GameObject hitObject)
    {
        if (!_allowSwipe)
            return;

        _allowSwipe = false;
        _lastSwpieTime = Time.time;

        Debug.Log($"_deltaOfLastPositionAndCurentPosition {_deltaOfLastPositionAndCurentPosition}");

        if (hitObject.tag == WholeAppleTag)
        {
            GameObject h1 = Instantiate(HalfApple, hitObject.transform.position, hitObject.transform.rotation);
            GameObject h2 = Instantiate(HalfApple, hitObject.transform.position, hitObject.transform.rotation);
            h1.transform.rotation *= Quaternion.Euler(0, -90, 0);
            h1.transform.Translate(0, 0, -0.25f);
            h1.GetComponent<Rigidbody>().velocity = hitObject.GetComponent<Rigidbody>().velocity;
            h1.GetComponent<Rigidbody>().AddTorque(_deltaOfLastPositionAndCurentPosition * SwipeTorque);
            h1.GetComponent<Rigidbody>().AddForce(_deltaOfLastPositionAndCurentPosition * SwipeForce);

            h2.transform.rotation *= Quaternion.Euler(180, -90, 0);
            h2.transform.Translate(0, 0, -0.25f);
            h2.GetComponent<Rigidbody>().velocity = hitObject.GetComponent<Rigidbody>().velocity;
            h2.GetComponent<Rigidbody>().AddTorque(_deltaOfLastPositionAndCurentPosition * SwipeTorque);
            h2.GetComponent<Rigidbody>().AddForce(_deltaOfLastPositionAndCurentPosition * SwipeForce);

            Destroy(hitObject);
        }
        else if (hitObject.tag == HalfAppleTag)
        {
            GameObject h1 = Instantiate(QuartreApple, hitObject.transform.position, hitObject.transform.rotation);
            GameObject h2 = Instantiate(QuartreApple, hitObject.transform.position, hitObject.transform.rotation);
            h1.transform.rotation *= Quaternion.Euler(0, 180, 90);
            h1.transform.Translate(0, 0, -0.11f);
            h1.GetComponent<Rigidbody>().velocity = hitObject.GetComponent<Rigidbody>().velocity;
            h1.GetComponent<Rigidbody>().AddTorque(_deltaOfLastPositionAndCurentPosition * SwipeTorque);
            h1.GetComponent<Rigidbody>().AddForce(_deltaOfLastPositionAndCurentPosition * SwipeForce);

            h2.transform.rotation *= Quaternion.Euler(-90, 180, 90);
            h2.transform.Translate(0, 0, -0.25f);
            h2.GetComponent<Rigidbody>().velocity = hitObject.GetComponent<Rigidbody>().velocity;
            h2.GetComponent<Rigidbody>().AddTorque(_deltaOfLastPositionAndCurentPosition * SwipeTorque);
            h2.GetComponent<Rigidbody>().AddForce(_deltaOfLastPositionAndCurentPosition * SwipeForce);

            Destroy(hitObject);
        }
    }
}
