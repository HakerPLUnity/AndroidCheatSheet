﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitCannon : MonoBehaviour
{
    public GameObject WholeApplePrefab;
    public Vector2 TimeShoot = new Vector2(1, 2);
    public float Force = 200;

    private float _nextTimeShoot = 0;

    void Start()
    {
        SetNextShootTime();
    }

    // Update is called once per frame
    void Update()
    {
        if (_nextTimeShoot > Time.time)
            return;

        SetNextShootTime();

        GameObject wholeApple = Instantiate(WholeApplePrefab, transform.position, Quaternion.identity);
        wholeApple.GetComponent<Rigidbody>()?.AddForce(transform.forward * Force);
    }

    private void SetNextShootTime()
    {
        _nextTimeShoot = Time.time + Random.Range(TimeShoot.x, TimeShoot.y);
    }
}
