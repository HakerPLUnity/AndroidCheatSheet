﻿using UnityEngine;

public class DestroyOnBecameInvisible : MonoBehaviour
{
    //method run when object is not visible on screen
    //we need mesh render for that
    void OnBecameInvisible()
    {
        //root is main object (first object in prefab)
        Destroy(gameObject.transform.root.gameObject);
    }
}
