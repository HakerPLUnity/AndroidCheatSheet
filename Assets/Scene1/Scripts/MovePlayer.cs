﻿using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float MovmentSpeed = 300;
    public Rigidbody2D PlayerRigidbody;

    private float _halfScreenWidth;
    // Start is called before the first frame update
    void Start()
    {
        _halfScreenWidth = Screen.width / 2;
    }

    void FixedUpdate()
    {
        int i = 0;

        while(Input.touchCount > i)
        {
            float positionX = Input.GetTouch(i).position.x;
            if (positionX >= _halfScreenWidth)
                Move(1.0f);
            else if (positionX < _halfScreenWidth)
                Move(-1.0f);

            i++;
        }
    }

    private void Move(float horizontalInput)
    {
        PlayerRigidbody.AddForce(new Vector2(horizontalInput * MovmentSpeed * Time.deltaTime, 0));
    }
}
