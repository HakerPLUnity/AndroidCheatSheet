﻿using System.Collections.Generic;
using UnityEngine;

public class DrawMultipleLine : MonoBehaviour
{
    public GameObject LinePrefab;

    //private GameObject _moveObject;
    //private Vector3 _startPositionLine;
    private Plane _plane;

    //key is Touch.fingerId
    private Dictionary<int, LineData> _lineDataDictionary;

    private void Start()
    {
        _lineDataDictionary = new Dictionary<int, LineData>();
        _plane = new Plane(Camera.main.transform.forward * -1, transform.position);
    }

    private void Update()
    {
        if (Input.touchCount == 0)
            return;

        TouchPhaseBegin();
        TouchPhaseEnd();

        //if (Input.GetTouch(0).phase == TouchPhase.Ended)
        //{
        //    if (_moveObject != null)
        //    {
        //        if (Vector3.Distance(_startPositionLine, _moveObject.transform.position) < 0.1)
        //            Destroy(_moveObject);
        //        _moveObject = null;
        //    }

        //    return;
        //}

        //if (Input.GetTouch(0).phase != TouchPhase.Began)
        //    return;

        //Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        //if (_plane.Raycast(ray, out float distanceHitRay))
        //{
        //    _moveObject = (GameObject)Instantiate(LinePrefab, ray.GetPoint(distanceHitRay), Quaternion.identity);
        //    _startPositionLine = ray.GetPoint(distanceHitRay);
        //}
    }

    private void FixedUpdate()
    {
        if (Input.touchCount == 0)
            return;

        TouchPhaseMove();

        //if (Input.GetTouch(0).phase != TouchPhase.Moved || _moveObject == null)
        //    return;

        ////return ray from camera to point on screen
        //Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        //float distanceHitRay;

        //if (_plane.Raycast(ray, out distanceHitRay))
        //    _moveObject.transform.position = ray.GetPoint(distanceHitRay);
    }

    private void TouchPhaseBegin()
    {
        Debug.Log($"Input.touchCount {Input.touchCount}");
        for (int i = 0; i < Input.touchCount; i++)
        {
            Debug.Log($"Input {i} phase {Input.GetTouch(i).phase}");
            if (Input.GetTouch(i).phase != TouchPhase.Began)
                continue;

            if (!_lineDataDictionary.ContainsKey(Input.GetTouch(i).fingerId))
            {
                LineData lineData = new LineData();
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                if (_plane.Raycast(ray, out float distanceHitRay))
                {
                    lineData.MoveObject = (GameObject)Instantiate(LinePrefab, ray.GetPoint(distanceHitRay), Quaternion.identity);
                    lineData.StartPositionLine = ray.GetPoint(distanceHitRay);
                }
                else
                {
                    Debug.Log($"ssss");
                    continue;
                }

                _lineDataDictionary.Add(Input.GetTouch(i).fingerId, lineData);

                Debug.Log($"Add new touch, count {_lineDataDictionary.Count}");
            }
        }
    }

    private void TouchPhaseMove()
    {
        foreach (KeyValuePair<int, LineData> entry in _lineDataDictionary)
        {
            Touch? touch = GetTouchByFingerId(entry.Key);
            if(touch == null)
                continue;

            //Debug.Log($"entry.Key.phase {touch?.phase}");
            if (touch?.phase != TouchPhase.Moved)
                continue;

            //Debug.Log($"Move object, count {_lineDataDictionary.Count}");
            Ray ray = Camera.main.ScreenPointToRay((Vector2)touch?.position);
            if (_plane.Raycast(ray, out float distanceHitRay))
                entry.Value.MoveObject.transform.position = ray.GetPoint(distanceHitRay);
        }
    }

    private void TouchPhaseEnd()
    {
        List<int> fingerIdToRemove = new List<int>();

        foreach (KeyValuePair<int, LineData> entry in _lineDataDictionary)
        {
            Touch? touch = GetTouchByFingerId(entry.Key);

            if (touch?.phase != TouchPhase.Ended)
                continue;

            Debug.Log($"Remove object, count {_lineDataDictionary.Count}");

            if (Vector3.Distance(entry.Value.StartPositionLine, entry.Value.MoveObject.transform.position) < 0.1)
                Destroy(entry.Value.MoveObject);

            fingerIdToRemove.Add(entry.Key);
        }

        foreach (int fingerId in fingerIdToRemove)
            _lineDataDictionary.Remove(fingerId);
    }

    private Touch? GetTouchByFingerId(int fingerId)
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (Input.GetTouch(i).fingerId == fingerId)
                return Input.GetTouch(i);
        }

        return null;
    }
}
