﻿using UnityEngine;

public class MoveObjectAboveSurface3D : MonoBehaviour
{
    public float DistanceRaycast = 100;
    public float GetUpObjectAboveGround = 1f;
    public float MultipleForce = 100f;

    #region Pick object data

    private GameObject _moveObject;
    private Plane _plane;
    private ChangeColor3D _changeColor;
    private Vector3 _offset;
    private Rigidbody _objectRigidbody;
    private Vector3 _positionFromLastFrame;
    private Vector3 _velocity; //to add force

    #endregion

    private void Update()
    {
        if (Input.touchCount == 0)
            return;

        

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            GameObject touchObject = GetTouchObject.GetGameObject3D(Input.GetTouch(0).position, DistanceRaycast);

            if (touchObject == null || touchObject.tag == "Map")
                return;

            _moveObject = touchObject;

            SetPickUpObjectRigidbodyData();

            //create plane after witch we move our object
            Vector3 position = _moveObject.transform.position;
            position.y -= 3f;
            _plane = new Plane(Vector3.up, position);
            _changeColor = _moveObject.GetComponent<ChangeColor3D>();

            if (_changeColor != null)
                _changeColor.ChangeSelect();

            //calculate offset
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            float distanceHitRay;

            //where we hit plane
            _plane.Raycast(ray, out distanceHitRay);
            _offset = _moveObject.transform.position - ray.GetPoint(distanceHitRay);
            _offset.y += GetUpObjectAboveGround;
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Ended && _moveObject != null)
        {
            if (_changeColor != null)
            {
                _changeColor.ChangeDefault();
                _changeColor = null;
            }

            SetPutDownObjectRigidbodyData();
            if (_objectRigidbody != null)
            {
                Debug.Log($"add force _velocity {_velocity} , _velocity * MultipleForce {_velocity * MultipleForce}");
                _objectRigidbody.AddForce(_velocity * MultipleForce);
                _objectRigidbody.AddTorque(_velocity * MultipleForce);
                _objectRigidbody = null;
            }

            _moveObject = null;

            return;
        }
    }

    private void FixedUpdate()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase != TouchPhase.Moved || _moveObject == null || _objectRigidbody == null)
            return;

        //return ray from camera to point on screen
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        float distanceHitRay;

        //Add script to dice where we will send new position and that script will be calculate position and collider
        if (_plane.Raycast(ray, out distanceHitRay))
        {
            _moveObject.transform.position = ray.GetPoint(distanceHitRay) + _offset;
            _velocity = _moveObject.transform.position - _positionFromLastFrame;
            _positionFromLastFrame = _moveObject.transform.position;
        }
    }

    private void SetPickUpObjectRigidbodyData()
    {
        _objectRigidbody = _moveObject.GetComponent<Rigidbody>();
        if (_objectRigidbody == null)
            return;

        _objectRigidbody.velocity = Vector3.zero;
        _objectRigidbody.useGravity = false;
        //_objectRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
    }

    private void SetPutDownObjectRigidbodyData()
    {
        if (_objectRigidbody == null)
            return;

        _objectRigidbody.useGravity = true;
        //_objectRigidbody.constraints = RigidbodyConstraints.None;
    }
}
