﻿using UnityEngine;

public static class GetTouchObject
{
    public static GameObject GetGameObject2D(Vector2 touchPoint, float distance)
    {
        Vector3 near;
        Vector3 far;

        CalculateCoordinateToWorldPoint(touchPoint, out near, out far);

        RaycastHit2D raycastHit2D = Physics2D.Raycast(near, far - near, distance);
        
        if (raycastHit2D.collider != null)
            return raycastHit2D.collider.gameObject;

        return null;
    }

    public static GameObject GetGameObject2DToMove(Vector2 touchPoint, float distance, out Vector2 hitPoint)
    {
        Vector3 near;
        Vector3 far;

        CalculateCoordinateToWorldPoint(touchPoint, out near, out far);

        RaycastHit2D raycastHit2D = Physics2D.Raycast(near, far - near, distance);

        if (raycastHit2D.collider != null)
        {
            hitPoint = raycastHit2D.point;
            return raycastHit2D.collider.gameObject;
        }

        hitPoint = Vector2.zero;
        return null;
    }

    public static GameObject GetGameObject3D(Vector2 touchPoint, float distance)
    {
        Vector3 near;
        Vector3 far;

        CalculateCoordinateToWorldPoint(touchPoint, out near, out far);
        RaycastHit raycastHit;

        if (Physics.Raycast(near, far - near, out raycastHit, distance))
            return raycastHit.collider.gameObject;

        return null;
    }

    public static void CalculateCoordinateToWorldPoint(Vector2 touchPoint, out Vector3 near, out Vector3 far)
    {
        //we get near plane camera touch cord
        Vector3 nearPointTouchLocal = new Vector3(touchPoint.x,
                                                  touchPoint.y,
                                                  Camera.main.nearClipPlane);

        //we get far plane camera touch cord
        Vector3 farPointTouchLocal = new Vector3(touchPoint.x,
                                                 touchPoint.y,
                                                 Camera.main.farClipPlane);

        //transform local touch position to world position
        near = Camera.main.ScreenToWorldPoint(nearPointTouchLocal);
        far = Camera.main.ScreenToWorldPoint(farPointTouchLocal);
    }
}
