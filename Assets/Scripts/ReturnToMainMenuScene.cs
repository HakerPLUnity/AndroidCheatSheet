﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToMainMenuScene : MonoBehaviour
{
    public string MainMenuSceneName = "";

    void Update()
    {
        if (DoubleTapDetected.IsDoubleTap() && !string.IsNullOrEmpty(MainMenuSceneName))
        {
            StartCoroutine(ClearInputTouch());
        }
    }

    private IEnumerator ClearInputTouch()
    {
        if (Input.touchCount != 0)
        {
            while (Input.touchCount > 0)
            {
                yield return null;
            }
        }

        SceneManager.LoadScene(MainMenuSceneName);
    }
}
