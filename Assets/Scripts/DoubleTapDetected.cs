﻿using UnityEngine;

public static class DoubleTapDetected
{
    private static float _lastPositionMagnitute = 0;
    private static float _lastClicktTime = 0;

    public static bool IsDoubleTap()
    {
        if (Input.touchCount != 1)
            return false;

        float maxTimeWait = 0.2f;          // time between touch
        float variancePosition = 3f;     // dostance between touch

        if (Input.GetTouch(0).phase != TouchPhase.Began)
            return false;

        //last time touch
        float deltaTimeTouch = Time.time;
        //deltaPosition diferent position between last touch
        float positionMagnitude = Input.GetTouch(0).position.magnitude;

        //Debug.Log($@"deltaTimeTouch = {deltaTimeTouch}, positionMagnitude = {positionMagnitude},
        //            _lastClicktTime = {_lastClicktTime}, _lastPositionMagnitute = {_lastPositionMagnitute}");
        //Debug.Log($@"time = {deltaTimeTouch - _lastClicktTime}, magnitute = {Mathf.Abs(positionMagnitude - _lastPositionMagnitute)}");

        if (_lastPositionMagnitute == 0 || _lastClicktTime == 0)
        {
            _lastPositionMagnitute = positionMagnitude;
            _lastClicktTime = deltaTimeTouch;
            return false;
        }

        bool returnResult = deltaTimeTouch > 0 && (deltaTimeTouch - _lastClicktTime) < maxTimeWait &&
                Mathf.Abs(positionMagnitude - _lastPositionMagnitute) < variancePosition;

        _lastPositionMagnitute = positionMagnitude;
        _lastClicktTime = deltaTimeTouch;

        return returnResult;
    }
}
