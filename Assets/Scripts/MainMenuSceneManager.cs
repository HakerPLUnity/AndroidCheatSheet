﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSceneManager : MonoBehaviour
{
    public string Game1SceneName = "";
    public string Game2SceneName = "";
    public string Game3SceneName = "";
    public string Game4SceneName = "";
    public string Game5SceneName = "";
    public string Game6SceneName = "";
    public string Game7SceneName = "";

    public void ExitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            return;
        #endif

        Application.Quit();
    }

    public void LoadSceneGame1()
    {
        if(string.IsNullOrEmpty(Game1SceneName))
        {
            Debug.Log("Game1SceneName is null or empty!!!");
            return;
        }

        SceneManager.LoadScene(Game1SceneName);
    }

    public void LoadSceneGame2()
    {
        if (string.IsNullOrEmpty(Game2SceneName))
        {
            Debug.Log("Game2SceneName is null or empty!!!");
            return;
        }

        SceneManager.LoadScene(Game2SceneName);
    }

    public void LoadSceneGame3()
    {
        if (string.IsNullOrEmpty(Game3SceneName))
        {
            Debug.Log("Game3SceneName is null or empty!!!");
            return;
        }

        SceneManager.LoadScene(Game3SceneName);
    }

    public void LoadSceneGame4()
    {
        if (string.IsNullOrEmpty(Game4SceneName))
        {
            Debug.Log("Game4SceneName is null or empty!!!");
            return;
        }

        SceneManager.LoadScene(Game4SceneName);
    }

    public void LoadSceneGame5()
    {
        if (string.IsNullOrEmpty(Game5SceneName))
        {
            Debug.Log("Game5SceneName is null or empty!!!");
            return;
        }

        SceneManager.LoadScene(Game5SceneName);
    }

    public void LoadSceneGame6()
    {
        if (string.IsNullOrEmpty(Game6SceneName))
        {
            Debug.Log("Game6SceneName is null or empty!!!");
            return;
        }

        SceneManager.LoadScene(Game6SceneName);
    }

    public void LoadSceneGame7()
    {
        if (string.IsNullOrEmpty(Game7SceneName))
        {
            Debug.Log("Game7SceneName is null or empty!!!");
            return;
        }

        SceneManager.LoadScene(Game7SceneName);
    }
}
