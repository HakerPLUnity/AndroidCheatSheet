﻿using UnityEngine;

public class MoveObject3D : MonoBehaviour
{
    public float DistanceRaycast = 100;

    private GameObject _moveObject;
    private Plane _plane;
    private ChangeColor3D _changeColor;
    private Vector3 _offset;

    private void Update()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            if (_changeColor != null)
            {
                _changeColor.ChangeDefault();
                _changeColor = null;
            }
            _moveObject = null;

            return;
        }

        if (Input.GetTouch(0).phase != TouchPhase.Began)
            return;

        GameObject touchObject = GetTouchObject.GetGameObject3D(Input.GetTouch(0).position, DistanceRaycast);

        if (touchObject == null)
            return;

        _moveObject = touchObject;
        //create plane after witch we move our object
        _plane = new Plane(Camera.main.transform.forward *- 1, _moveObject.transform.position);
        _changeColor = _moveObject.GetComponent<ChangeColor3D>();

        if (_changeColor != null)
            _changeColor.ChangeSelect();

        //calculate offset
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        float distanceHitRay;

        //where we hit plane
        _plane.Raycast(ray, out distanceHitRay);
        _offset = _moveObject.transform.position - ray.GetPoint(distanceHitRay);
    }

    private void FixedUpdate()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase != TouchPhase.Moved || _moveObject == null)
            return;

        //return ray from camera to point on screen
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        float distanceHitRay;

        if (_plane.Raycast(ray, out distanceHitRay))
            _moveObject.transform.position = ray.GetPoint(distanceHitRay) + _offset;
    }
}
