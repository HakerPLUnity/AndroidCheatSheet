﻿using UnityEngine;

public class ChangeColor3D : MonoBehaviour
{
    public Material DefaultColor;
    public Material SelectColor;

    private MeshRenderer _meshRenderer;

    private void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();

        _meshRenderer.material = DefaultColor;
    }

    public void ChangeDefault()
    {
        _meshRenderer.material = DefaultColor;
    }

    public void ChangeSelect()
    {
        _meshRenderer.material = SelectColor;
    }
}