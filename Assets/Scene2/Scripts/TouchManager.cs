﻿using UnityEngine;

public class TouchManager : MonoBehaviour
{
    public float DistanceRaycast = 100;

    void Update()
    {
        if (Input.touchCount == 0)
            return;

        int i = 0;

        while(i < Input.touchCount)
        {
            if(Input.GetTouch(i).phase != TouchPhase.Began)
            {
                i++;
                continue;
            }

            GameObject touchObject = GetTouchObject.GetGameObject2D(Input.GetTouch(i).position, DistanceRaycast);

            if (touchObject == null)
                return;

            ChangeColor changeColor = touchObject.GetComponent<ChangeColor>();
            if (changeColor == null)
                return;

            changeColor.Change();

            i++;
        }
    }
}
