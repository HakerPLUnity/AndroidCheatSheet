﻿using System.Collections;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public Color DefaultColor = Color.red;
    public Color SelectColor  = Color.green;

    private bool _isCoroutineOn;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.color = DefaultColor;
        _isCoroutineOn = false;
    }

    public void Change()
    {
        if (_isCoroutineOn)
            return;

        _isCoroutineOn = true;

        StartCoroutine(ChangeCoroutine());
    }

    private IEnumerator ChangeCoroutine()
    {
        float startTime = Time.time;
        spriteRenderer.color = SelectColor;

        while (Time.time - startTime < 2)
        {
            yield return null;
        }

        spriteRenderer.color = DefaultColor;
        _isCoroutineOn = false;
    }
}
