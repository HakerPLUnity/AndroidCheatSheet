﻿using UnityEngine;

namespace Scene3
{
    public class ChangeColor : MonoBehaviour
    {
        public Color DefaultColor = Color.red;
        public Color SelectColor = Color.green;

        private SpriteRenderer spriteRenderer;

        private void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();

            spriteRenderer.color = DefaultColor;
        }

        public void ChangeDefault()
        {
            spriteRenderer.color = DefaultColor;
        }

        public void ChangeSelect()
        {
            spriteRenderer.color = SelectColor;
        }
    }
}
