﻿using UnityEngine;

public class MoveObject2D : MonoBehaviour
{
    public float DistanceRaycast = 100;
    private GameObject _moveObject;

    private Scene3.ChangeColor _changeColor;
    private Vector3 _offset;

    private void Update()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            if (_changeColor != null)
            { 
                _changeColor.ChangeDefault();
                _changeColor = null;
            }
            _moveObject = null;

            return;
        }

        if (Input.GetTouch(0).phase != TouchPhase.Began)
            return;

        Vector2 hitPoint;
        GameObject touchObject = GetTouchObject.GetGameObject2DToMove(Input.GetTouch(0).position, DistanceRaycast, out hitPoint);

        if (touchObject == null)
            return;

        _moveObject = touchObject;

        _changeColor = _moveObject.GetComponent<Scene3.ChangeColor>();

        if (_changeColor != null)
            _changeColor.ChangeSelect();

        _offset = _moveObject.transform.position - new Vector3(hitPoint.x, hitPoint.y, 0);
    }

    private void FixedUpdate()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase != TouchPhase.Moved || _moveObject == null)
            return;

        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

        _moveObject.transform.position = new Vector3(worldPosition.x, worldPosition.y, _moveObject.transform.position.z) + _offset;
    }
}
